module TrialsHelper
  def saas_trial_entrypoint_url
    "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=free-trial"
  end
end
